pub mod sequence_parser;

use sequence_parser::{
    Action,
    Operation,
};

use std::time::Duration;
use std::thread;

use serialport::{self, SerialPort};


trait Engine {
    fn perform(&mut self, op: &Operation) -> ();
}


struct NopEngine {
}

impl Engine for NopEngine {
    fn perform(&mut self, op: &Operation) -> () {
        match op.action {
            Action::Sleep => {
            },
            Action::On  => {
                println!("Turning nothing on.");
            },
            Action::Off => {
                println!("Turning nothing off.");
            },
        };
        println!("Waiting for {}.", humantime::format_duration(op.duration));
        thread::sleep(op.duration);
    }
}


struct SerialPortEngine {
    port: Box<dyn SerialPort>,
}

impl Engine for SerialPortEngine {
    fn perform(&mut self, op: &Operation) -> () {
        match op.action {
            Action::Sleep => {
            },
            Action::On  => {
                println!("Turning USB on.");
                self.port.write("D8H".as_bytes()).expect("Unable to write");
            },
            Action::Off => {
                println!("Turning USB off.");
                self.port.write("D8L".as_bytes()).expect("Unable to write");
            },
        };
        println!("Waiting for {}.", humantime::format_duration(op.duration));
        thread::sleep(op.duration);
    }
}


#[derive(Debug)]
pub struct Settings {
    pub device:         String,
    pub operations:     Vec<Operation>,
    pub iterations:     u32,
    pub dry_run:        bool,
}

pub fn run(settings: Settings) {
    println!("Opening device '{}'", settings.device);

    let mut engine : Box<dyn Engine> =
        if settings.dry_run {
            Box::new(NopEngine {})
        } else {
            Box::new(
                SerialPortEngine {
                    port: serialport::new(&settings.device, 9600)
                        .timeout(Duration::from_millis(10))
                        .open()
                        .expect("Unable to open port")
                }
            )
        };

    for i in 1..(settings.iterations + 1) {
        println!("Iteration {} of {}", i, settings.iterations);

        for op in &settings.operations {
            println!("Running action: {op}");
            engine.perform(op);
        }
    }
}
