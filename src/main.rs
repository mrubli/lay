use clap::Parser;

#[derive(Parser, Debug)]
#[command(
    name = "lay",
    about = "Control ACON Box Plus relays.",
    author,
    version,
)]
#[clap(disable_help_flag = true)]
struct CmdlineArgs {
    /// Print help
    #[arg(short, long = "of-the-land", visible_alias = "help", action = clap::ArgAction::Help, required = false)]
    help: (),
    /// Select the UART device name
    #[arg(short, long, default_value = "/dev/ttyUSB0")]
    device: String,
    /// Command sequence to execute
    #[arg(short, long)]
    sequence: Option<String>,
    /// Number of times the sequence should be run
    #[arg(short, long, default_value_t = 1)]
    iterations: u32,
    /// Don't send any commands to the UART port
    #[arg(long)]
    dry_run: bool,
}

fn main() {
    let settings = parse_arguments();
    println!("Settings: {:?}", settings);

    lay::run(settings);
}

fn parse_arguments() -> lay::Settings {
    let args = CmdlineArgs::parse();

    let mut settings = lay::Settings{
        device:         args.device,
        operations:     vec![],
        iterations:     args.iterations,
        dry_run:        args.dry_run,
    };

    if let Some(seq) = args.sequence {
        println!("Parsing sequence: {}", seq);
        match lay::sequence_parser::parse_sequence(&seq) {
            Ok(ops) => settings.operations = ops,
            Err(e) => panic!("Unable to parse sequence: '{e}'"),
        }
    }

    settings
}
