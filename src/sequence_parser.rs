//! Parse sequences of actions.
//!
//! # Grammar
//!
//! The recognized sequenced are described by the following grammar:
//!
//! ```text
//! Unit = "ms"
//!      | "s"
//!      | "min"
//!      | "h"
//!
//! Duration = u64 Unit
//!
//! Action = "sleep"
//!        | "on"
//!        | "off"
//!
//! Operation = Action
//!           | Action <whitespace> { <whitespace> } Duration
//!
//! Sequence = ""
//!          | Operation [ { <whitespace> } ',' { <whitespace> } Operation ]
//! ```
//!
//! # Examples
//!
//! `on 7s, off 3s`
//!
//! On for 7 seconds, off for 3 seconds.
//!
//! `on, sleep 7000ms, off, sleep 3s`
//!
//! Same as above, just using a different style.

use std::time::Duration;

use nom::{
    Finish,
    IResult,
    branch::alt,
    bytes::complete::tag,
    character::complete::{
        space0,
        space1,
        u64,
    },
    combinator::{
        all_consuming,
        map,
        value,
    },
    multi::separated_list0,
    sequence::{
        delimited,
        pair,
        separated_pair,
    },
};


/// Describes an action that is performed as part of executing an operation.
#[derive(Clone, Debug, PartialEq)]
pub enum Action {
    /// Wait without doing anything.
    Sleep,
    /// Turn a relay on.
    On,
    /// Turn a relay off.
    Off,
}

impl std::fmt::Display for Action {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(fmt, "{}",
            match self {
                Action::Sleep => "Sleep",
                Action::On => "On",
                Action::Off => "Off",
            }
        )
    }
}


/// Describes an operation that is executed as part of a sequence.
#[derive(Debug, PartialEq)]
pub struct Operation {
    /// The action to perform.
    pub action: Action,
    /// The duration of the action or how long to wait after performing the action.
    pub duration: Duration,
}

impl std::fmt::Display for Operation {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(fmt, "{} for {}s", self.action, self.duration.as_secs())
    }
}


// Action = "sleep"
//        | "on"
//        | "off"
fn parse_action(input: &str) -> IResult<&str, Action> {
    alt((
        value(Action::Sleep, tag("sleep")),
        value(Action::On, tag("on")),
        value(Action::Off, tag("off")),
    ))(input)
}

// Unit = "ms"
//      | "s"
//      | "min"
//      | "h"
fn parse_duration_unit(input: &str) -> IResult<&str, &str> {
    alt((
        tag("ms"),
        tag("s"),
        tag("min"),
        tag("h")
    ))(input)
}

fn make_duration(p: (u64, &str)) -> Duration {
    let (count, unit) = p;
    match unit {
        "ms"    => Duration::from_millis(count),
        "s"     => Duration::from_secs(count),
        "min"   => Duration::from_secs(count * 60),
        "h"     => Duration::from_secs(count * 60 * 60),
        _       => panic!("Invalid duration unit: '{unit}'"),
    }
}

// Duration = u64 Unit
fn parse_duration(input: &str) -> IResult<&str, Duration> {
    map(
        pair(u64, parse_duration_unit),
        make_duration
    )(input)
}

// Operation = Action
//           | Action <whitespace> { <whitespace> } Duration
fn parse_operation(input: &str) -> IResult<&str, Operation> {
    let (remaining, parsed) =
        alt((
            separated_pair(
                parse_action,
                space1,
                parse_duration
            ),
            map(parse_action, |a: Action| (a, Duration::from_secs(0)))
        ))(input)?;
    let (action, duration) = parsed;
    Ok((remaining, Operation{ action: action, duration: duration }))
}


/// Parses a string containing a sequence of operations.
///
/// If successful, this returns a vector of [`Operation`] structs.
/// The empty string is a valid sequence and results in an empty vector.
///
/// On error the result contains a string describing the encountered problem.
//
// Sequence = ""
//          | Operation [ { <whitespace> } ',' { <whitespace> } Operation ]
pub fn parse_sequence(seq: &String) -> Result<Vec<Operation>, String> {
    let parse_result = all_consuming(separated_list0(
        delimited(space0, tag(","), space0),
        parse_operation
    ))(seq);
    let (remaining_input, ops) = parse_result
        .finish()
        .map_err(|e: nom::error::Error<&str>| e.to_string())?;
    assert!(remaining_input.is_empty());
    Ok(ops)
}


#[cfg(test)]
mod sequence_parser_tests {
    use super::*;

    #[test]
    fn parse_actions() {
        assert_eq!(parse_action("sleep"), Ok(("", Action::Sleep)));
        assert_eq!(parse_action("on"   ), Ok(("", Action::On   )));
        assert_eq!(parse_action("off"  ), Ok(("", Action::Off  )));

        assert!(all_consuming(parse_action)("sleepy").is_err());

        assert!(all_consuming(parse_action)("asleep").is_err());
        assert!(all_consuming(parse_action)("doze").is_err());
    }

    #[test]
    fn parse_duration_units() {
        assert_eq!(parse_duration_unit("ms" ), Ok(("", "ms" )));
        assert_eq!(parse_duration_unit("s"  ), Ok(("", "s"  )));
        assert_eq!(parse_duration_unit("min"), Ok(("", "min")));
        assert_eq!(parse_duration_unit("h"  ), Ok(("", "h"  )));

        assert!(all_consuming(parse_duration_unit)("sx").is_err());

        assert!(all_consuming(parse_duration_unit)("xs").is_err());
        assert!(all_consuming(parse_duration_unit)("foo").is_err());
    }

    #[test]
    fn make_durations() {
        assert_eq!(make_duration(( 0, "ms" )), Duration::from_millis(0           ));
        assert_eq!(make_duration((42, "ms" )), Duration::from_millis(42          ));
        assert_eq!(make_duration((42, "s"  )), Duration::from_secs  (42          ));
        assert_eq!(make_duration((42, "min")), Duration::from_secs  (42 * 60     ));
        assert_eq!(make_duration((42, "h"  )), Duration::from_secs  (42 * 60 * 60));
    }

    #[test]
    fn parse_durations() {
        assert_eq!(parse_duration("42ms" ), Ok(("", Duration::from_millis(42          ))));
        assert_eq!(parse_duration("42s"  ), Ok(("", Duration::from_secs  (42          ))));
        assert_eq!(parse_duration("42min"), Ok(("", Duration::from_secs  (42 * 60     ))));
        assert_eq!(parse_duration("42h"  ), Ok(("", Duration::from_secs  (42 * 60 * 60))));

        assert!(all_consuming(parse_duration)("64squirrels"  ).is_err());
        assert!(all_consuming(parse_duration)("10hours"  ).is_err());

        assert!(parse_duration("42 s").is_err());
    }

    #[test]
    fn parse_operations() {
        assert_eq!(
            parse_operation("on 13s"),
            Ok(("", Operation{ action: Action::On, duration: Duration::from_secs(13) }))
        );
        assert_eq!(
            parse_operation("sleep 420ms"),
            Ok(("", Operation{ action: Action::Sleep, duration: Duration::from_millis(420) }))
        );

        // Allow extra whitespace before duration
        assert_eq!(
            parse_operation("off  3h"),
            Ok(("", Operation{ action: Action::Off, duration: Duration::from_secs(3 * 3600) }))
        );
        assert_eq!(
            parse_operation("off\t3h"),
            Ok(("", Operation{ action: Action::Off, duration: Duration::from_secs(3 * 3600) }))
        );

        assert_eq!(
            parse_operation("off"),
            Ok(("", Operation{ action: Action::Off, duration: Duration::from_secs(0) }))
        );

        // Unknown actions
        assert!(all_consuming(parse_operation)("asleep 13s").is_err());
        assert!(all_consuming(parse_operation)("sleepy 13s").is_err());

        // Invalid durations
        assert!(all_consuming(parse_operation)("on 13serpent").is_err());
        assert!(all_consuming(parse_operation)("on 13").is_err());

        // Whitespace errors
        assert!(all_consuming(parse_operation)("on 13s ").is_err());
        assert!(all_consuming(parse_operation)("off\n3h").is_err());

        // Nonsense
        assert!(all_consuming(parse_operation)("huh").is_err());
        assert!(all_consuming(parse_operation)("").is_err());
    }

    #[test]
    fn parse_sequences() {
        assert_eq!(
            parse_sequence(&"off 42min".to_string()),
            Ok(vec![
                Operation{ action: Action::Off, duration: Duration::from_secs(42 * 60) },
            ])
        );
        assert_eq!(
            parse_sequence(&"on 7s,off 3s".to_string()),
            Ok(vec![
                Operation{ action: Action::On,  duration: Duration::from_secs(7) },
                Operation{ action: Action::Off, duration: Duration::from_secs(3) },
            ])
        );
        assert_eq!(
            parse_sequence(&"on,sleep 7s,off,sleep 3s".to_string()),
            Ok(vec![
                Operation{ action: Action::On,     duration: Duration::from_secs(0) },
                Operation{ action: Action::Sleep,  duration: Duration::from_secs(7) },
                Operation{ action: Action::Off,    duration: Duration::from_secs(0) },
                Operation{ action: Action::Sleep,  duration: Duration::from_secs(3) },
            ])
        );

        // Separation variations
        assert_eq!(
            parse_sequence(&"on 7s ,off 3s".to_string()),
            Ok(vec![
                Operation{ action: Action::On,  duration: Duration::from_secs(7) },
                Operation{ action: Action::Off, duration: Duration::from_secs(3) },
            ])
        );
        assert_eq!(
            parse_sequence(&"on 7s, off 3s".to_string()),
            Ok(vec![
                Operation{ action: Action::On,  duration: Duration::from_secs(7) },
                Operation{ action: Action::Off, duration: Duration::from_secs(3) },
            ])
        );
        assert_eq!(
            parse_sequence(&"on 7s , off 3s".to_string()),
            Ok(vec![
                Operation{ action: Action::On,  duration: Duration::from_secs(7) },
                Operation{ action: Action::Off, duration: Duration::from_secs(3) },
            ])
        );
        assert_eq!(
            parse_sequence(&"on 7s   , off 3s".to_string()),
            Ok(vec![
                Operation{ action: Action::On,  duration: Duration::from_secs(7) },
                Operation{ action: Action::Off, duration: Duration::from_secs(3) },
            ])
        );

        // Empty sequence
        assert_eq!( parse_sequence(&"".to_string()), Ok(vec![]) );

        // Empty actions
        assert!(parse_sequence(&"on 7s,off 3s,".to_string()).is_err());
        assert!(parse_sequence(&",on 7s,off 3s".to_string()).is_err());

        // Invalid actions
        assert!(parse_sequence(&"orz 42s".to_string()).is_err());
    }
}
